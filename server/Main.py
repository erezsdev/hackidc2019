import json
import os.path

import flask
from flask import Flask, send_from_directory
from flask import request as flask_request
from flask_cors import CORS
from VoiceToText import VoiceToText
from TextToVoice import TextToVoice

app = Flask('Hack')
CORS(app)
subscription_key = "3b1e20284fc445f0822911c6dcc8216c"
txt_to_convert = ''
SUCCESS = {'status': 'success'}
FAILED = {'status': 'failed'}


@app.route('/voiceToText/<string:file_name>/<string:voice_font>/<string:mode>')
def convert_voice_to_text(file_name, voice_font, mode):
    voice_to_text = VoiceToText(r"C:\Users\erez_1ezdhx1\Downloads\%s" % file_name)
    txt_to_convert = voice_to_text.convertToText()
    convert_text_to_voice_trump(txt_to_convert, file_name, voice_font, mode)
    return json.dumps(txt_to_convert)


@app.route('/textToVoice/<string:voice_font>')
def convert_text_to_voice_trump(txt_to_convert, file_name, voice_font, mode):
    text_to_voice = TextToVoice(subscription_key, txt_to_convert)
    text_to_voice.get_token()
    text_to_voice.save_audio(voice_font, file_name=file_name, mode=mode)
    return json.dumps(SUCCESS)

@app.route('/textToVoice/obama')
def convert_text_to_voice_obama(txt_to_convert, file_name):
    text_to_voice = TextToVoice(subscription_key, txt_to_convert)
    text_to_voice.get_token()
    text_to_voice.save_audio(voice_font='obama_new', file_name=file_name)
    return json.dumps(SUCCESS)

@app.route('/getFile/<string:file_name>')
def get_file(file_name):
    return send_from_directory(r"C:\erez\hackidc\voices", file_name)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
