import os
import requests
import time
from xml.etree import ElementTree


class TextToVoice(object):
    def __init__(self, subscription_key, txt_to_convert="I am speechless"):
        self.subscription_key = subscription_key
        self.tts = txt_to_convert
        self.timestr = time.strftime("%Y%m%d-%H%M")
        self.access_token = None

    def get_token(self):
        fetch_token_url = "https://westus.api.cognitive.microsoft.com/sts/v1.0/issueToken"
        headers = {
            'Ocp-Apim-Subscription-Key': self.subscription_key
        }
        response = requests.post(fetch_token_url, headers=headers)
        self.access_token = str(response.text)

    def save_audio(self, voice_font='trump_test_mid_2', file_name='', mode='natural'):
        prec = 0
        uniq_url = r'https://westus.voice.speech.microsoft.com/cognitiveservices/v1?deploymentId=b5571864-aa47-4962-8686-023692615d06'
        default_url = r'https://westus.tts.speech.microsoft.com/cognitiveservices/v1'

        constructed_url = uniq_url if 'trump' in voice_font else default_url
        headers = {
            'Authorization': 'Bearer ' + self.access_token,
            'Content-Type': 'application/ssml+xml',
            'X-Microsoft-OutputFormat': 'riff-24khz-16bit-mono-pcm',
            'User-Agent': 'YOUR_RESOURCE_NAME'
        }
        xml_body = ElementTree.Element('speak', version='1.0')
        xml_body.set('{http://www.w3.org/XML/1998/namespace}lang', 'en-us')
        voice = ElementTree.SubElement(xml_body, 'voice')
        voice.set('{http://www.w3.org/XML/1998/namespace}lang', 'en-US')
        print('voice_font: %s' % voice_font)
        print(constructed_url)
        voice.set('name', voice_font)

        prosody = ElementTree.SubElement(voice, 'prosody')
        if mode == 'Nervous':
            prec = +15
        elif mode == 'calm':
            prec = -20
        prosody.set('rate', "%s" % prec + ".00%")
        if mode == 'Nervous':
            self.tts = self.tts.replace(',', '!').replace('.', '!')
        print(self.tts)
        prosody.text = self.tts

        body = ElementTree.tostring(xml_body)
        print(body)

        response = requests.post(constructed_url, headers=headers, data=body)
        if response.status_code == 200:
            with open(r'C:\erez\hackidc\voices\%s_%s.wav' % (file_name, mode), 'wb') as audio:
                audio.write(response.content)
                print("\nStatus code: " + str(response.status_code) + "\nYour TTS is ready for playback.\n")
        else:
            print("\nStatus code: " + str(
                response.status_code) + "\nSomething went wrong. Check your subscription key and headers.\n")



if __name__ == "__main__":
    subscription_key = "3b1e20284fc445f0822911c6dcc8216c"
    app = TextToVoice(subscription_key)
    app.get_token()
    # app.print_t()
    app.save_audio()