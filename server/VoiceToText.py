import speech_recognition as sr
import requests

class VoiceToText(object):
    def __init__(self, file_path):
        self.r = sr.Recognizer()
        self.file_path = file_path

    def convertToText(self):
        try:
            with sr.AudioFile(self.file_path) as source:
                audio = self.r.record(source)  # read the entire audio file
        except Exception as ex:
            print('Msg: %s' % ex)
            return "Failed to open voice file."

        try:
            return self.add_punctuation(self.r.recognize_google(audio))
        except sr.UnknownValueError:
            return("Miri could not understand audio")
        except sr.RequestError as e:
            return("Miri error; {0}".format(e))

    def add_punctuation(self,text):
        return requests.post('http://bark.phon.ioc.ee/punctuator',data={'text':text}).content.decode()
