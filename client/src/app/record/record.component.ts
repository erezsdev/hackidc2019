import { Component, OnInit } from '@angular/core';
import { ConverterService } from '../converter.service';

declare var window;
@Component({
  selector: 'app-record',
  templateUrl: './record.component.html',
  styleUrls: ['./record.component.css']
})
export class RecordComponent implements OnInit {

  recording = false;
  blob: any;
  fileName: string;
  predict: string;
  loading = false;
  outputVoice: string = 'trump_test_mid_2';
  outputIntonation: string = 'natural';

  constructor(private converter: ConverterService) { }

  ngOnInit() {
  }

  toggleRecording() {
    if (this.recording) {
      setTimeout(() => {
        this.createNewFile()
      }, 500)
    }
    this.recording = !this.recording;
  }

  play() {
    var audio = new Audio("http://localhost:5000/getFile/" + this.fileName + "_" + this.outputIntonation + ".wav");
    audio.load();
    audio.play();
  }

  createNewFile() {
    this.loading = true;
    this.blob = window.recordBlob;
    document.getElementById("save").click();
    var fileName = window.executedFileName;
    if (fileName) {
      console.log("Blob saved successfully! " + window.executedFileName)
      this.fileName = window.executedFileName;
      this.converter.voiceToText(fileName, this.outputVoice, this.outputIntonation).then(res => {
        this.predict = res.toString();
        this.loading = false;
      })
    }
    else {
      console.log("No file name..")
    }
  }

  onNewType() {
    if (this.fileName) {
      this.refreshFile()
    }
  }

  refreshFile() {
    this.loading = true;
    this.converter.voiceToText(this.fileName, this.outputVoice, this.outputIntonation).then(res => {
      this.predict = res.toString();
      this.loading = false;
    })
  }


}
