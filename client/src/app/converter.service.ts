import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConverterService {

  constructor(private http: HttpClient) {

  }

  textToVoice(voiceName?) {
    if(!voiceName) {
      voiceName = '/trump'
    }
    else{
      voiceName = "/"+voiceName;
    }
    return new Promise(resolve => {
      this.http.get("http://localhost:5000/textToVoice" + voiceName).subscribe(res => {
        return resolve(res);
      })
    })
  }


  voiceToText(fileName,voice_font,voice_intonation) {
    return new Promise(resolve => {
      this.http.get("http://localhost:5000/voiceToText/" + fileName+"/"+voice_font+"/"+voice_intonation).subscribe(res => {
        return resolve(res);
      })
    })
  }
}
