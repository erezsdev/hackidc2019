var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
var RecordComponent = /** @class */ (function () {
    function RecordComponent(sanitizer) {
        this.sanitizer = sanitizer;
        this.recording = false;
    }
    RecordComponent.prototype.ngOnInit = function () {
    };
    RecordComponent.prototype.toggleRecording = function () {
        var _this = this;
        if (this.recording) {
            setTimeout(function () {
                _this.blob = window.recordBlob;
                document.getElementById("save").click();
                console.log("Blob saved successfully! " + window.executedFileName);
                // FileSaver.saveAs(blob)
            }, 200);
        }
        this.recording = !this.recording;
    };
    RecordComponent = __decorate([
        Component({
            selector: 'app-record',
            templateUrl: './record.component.html',
            styleUrls: ['./record.component.css']
        }),
        __metadata("design:paramtypes", [DomSanitizer])
    ], RecordComponent);
    return RecordComponent;
}());
export { RecordComponent };
//# sourceMappingURL=record.component.js.map